library(shiny)
library(magick)

myUI <- fluidPage(
  titlePanel("Testing File upload"),

  sidebarLayout(
    sidebarPanel(
      fileInput('file_input', 'upload file ( . pdf format only)', accept = c("application/pdf", ".pdf", "application/vnd.openxmlformats-officedocument.presentationml.presentation", ".pptx", ".ppt", "application/vnd.ms-powerpoint"))
    ),

    mainPanel(
      uiOutput("pdfview"),
      uiOutput("myImage"),
      uiOutput("myPPT")
    )
  )
)