library(shiny)
library(magick)
source("ui.R", local = TRUE)
source("server.R")


shinyApp(
  ui = myUI,
  server = myServer
)

shinyApp(ui, server)